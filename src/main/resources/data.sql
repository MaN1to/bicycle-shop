INSERT INTO bicycle (brand, wheel_diameter, brake_type, frame_size)
VALUES
('Yamaha', '29"', 'HYDRAULIC', '20"'),
('Suzuki', '27.5"', 'MECHANICAL', '19"');