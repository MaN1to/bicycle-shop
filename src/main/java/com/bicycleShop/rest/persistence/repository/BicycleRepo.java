package com.bicycleShop.rest.persistence.repository;

import com.bicycleShop.rest.persistence.domain.Bicycle;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BicycleRepo extends JpaRepository<Bicycle, Long> {

  @Override
  <S extends Bicycle> S saveAndFlush(S entity);

  @Override
  List<Bicycle> findAll();
}
