package com.bicycleShop.rest.persistence.domain;

import com.bicycleShop.rest.domain.request.BicycleRequest;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "bicycle")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Bicycle implements Serializable {

  private static final long serialVersionUID = 4090329215575690403L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private int id;
  //Marca
  private String brand;
  //Diametro de la rueda
  private String wheelDiameter;
  //Tipo de freno
  private String brakeType;
  //Tamaño del cuadro
  private String frameSize;


  /**
   * @param bicycleRequest
   * @return
   */
  public static Bicycle buildBicycleEntity(BicycleRequest bicycleRequest) {
    return Bicycle.builder()
        .brand(bicycleRequest.getBrand())
        .wheelDiameter(bicycleRequest.getWheelDiameter().replace("\\", ""))
        .brakeType(bicycleRequest.getBrakeType().toUpperCase())
        .frameSize(bicycleRequest.getFrameSize().replace("\\", ""))
        .build();
  }
}
