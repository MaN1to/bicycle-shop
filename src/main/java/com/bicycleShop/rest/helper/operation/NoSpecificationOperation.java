package com.bicycleShop.rest.helper.operation;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import com.bicycleShop.rest.domain.request.BicyclesRequest;
import com.bicycleShop.rest.domain.response.BicycleResponse;
import com.bicycleShop.rest.helper.Strategy;
import org.springframework.stereotype.Component;

@Component
public class NoSpecificationOperation implements Strategy {

  @Override
  public boolean filterBicycle(BicyclesRequest bicyclesRequest,
      BicycleResponse bicycleResponses) {
    return !nonNull(bicyclesRequest) || isEmpty(bicyclesRequest);
  }

  private boolean isEmpty(BicyclesRequest bicyclesRequest) {
    if(nonNull(bicyclesRequest)){
      return isNull(bicyclesRequest.getBrand()) && isNull(bicyclesRequest.getFrameSize()) && isNull(bicyclesRequest.getWheelDiameter());
    }
    return false;
  }
}
