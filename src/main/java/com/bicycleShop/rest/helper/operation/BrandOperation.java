package com.bicycleShop.rest.helper.operation;

import static java.util.Objects.nonNull;

import com.bicycleShop.rest.domain.request.BicyclesRequest;
import com.bicycleShop.rest.domain.response.BicycleResponse;
import com.bicycleShop.rest.helper.Strategy;
import org.springframework.stereotype.Component;

@Component
public class BrandOperation implements Strategy {

  @Override
  public boolean filterBicycle(BicyclesRequest bicyclesRequest,
      BicycleResponse bicycleResponses) {
    if(nonNull(bicyclesRequest.getBrand())){
      return isBicycleEquals(bicyclesRequest, bicycleResponses);
    }
    return false;
  }

  private boolean isBicycleEquals(BicyclesRequest bicyclesRequest,
      BicycleResponse bicycleResponses) {

    return bicyclesRequest.getBrand().equals(bicycleResponses.getBrand());
  }
}
