package com.bicycleShop.rest.helper.operation;

import static java.util.Objects.nonNull;

import com.bicycleShop.rest.domain.request.BicyclesRequest;
import com.bicycleShop.rest.domain.response.BicycleResponse;
import com.bicycleShop.rest.helper.Strategy;
import org.springframework.stereotype.Component;

@Component
public class FrameOperation implements Strategy {

  @Override
  public boolean filterBicycle(BicyclesRequest bicyclesRequest,
      BicycleResponse bicycleResponses) {
    if(nonNull(bicyclesRequest.getFrameSize())){
      return isBicycleEquals(bicyclesRequest, bicycleResponses);
    }
    return false;
  }

  private boolean isBicycleEquals(BicyclesRequest bicyclesRequest,
      BicycleResponse bicycleResponses) {

    return bicyclesRequest.getFrameSize().equals(bicycleResponses.getFrameSize());
  }
}
