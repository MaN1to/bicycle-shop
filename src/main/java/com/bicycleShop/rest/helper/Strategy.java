package com.bicycleShop.rest.helper;

import com.bicycleShop.rest.domain.request.BicyclesRequest;
import com.bicycleShop.rest.domain.response.BicycleResponse;

public interface Strategy {
  boolean filterBicycle(BicyclesRequest bicyclesRequest, BicycleResponse bicycleResponses);
}
