package com.bicycleShop.rest.controllers;

import static java.util.Objects.isNull;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_CREATED;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;

import com.bicycleShop.rest.domain.request.BicycleRequest;
import com.bicycleShop.rest.domain.request.BicyclesRequest;
import com.bicycleShop.rest.domain.response.BicycleResponse;
import com.bicycleShop.rest.domain.response.BicyclesResponse;
import com.bicycleShop.rest.service.interfaces.BicycleService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shop")
public class ShopController {

  private final BicycleService bicycleService;

  public ShopController(BicycleService bicycleService) {
    this.bicycleService = bicycleService;
  }

  @ApiOperation(
      value = "It will add a new bicycle into the database"
  )
  @ApiResponses(value = {
      @ApiResponse(code = SC_CREATED, message = "Created"),
      @ApiResponse(code = SC_BAD_REQUEST, message = "Bad Request"),
      @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error")})
  @ResponseStatus(value = HttpStatus.CREATED)
  @PostMapping(
      path = "/create",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseEntity<BicycleResponse> bicycle(
      @ApiParam(name = "Bicycle Request", value = "BicycleRequest Dto", required = true)
      @Valid @RequestBody BicycleRequest bicycleRequest){
    BicycleResponse bicycleResponse = bicycleService.bicycle(bicycleRequest);

    if(isNull(bicycleResponse)){
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<>(bicycleResponse, HttpStatus.CREATED);
  }

  @ApiOperation(
      value = "It will return a group of 3 bicycle",
      response = BicycleRequest.class
  )
  @ApiResponses(value = {
      @ApiResponse(code = SC_OK, message = "OK"),
      @ApiResponse(code = SC_BAD_REQUEST, message = "Bad Request."),
      @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error")})
  @ResponseStatus(value = HttpStatus.OK)
  @PostMapping(path = "/obtain",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<BicyclesResponse> obtainBicycle(
      @ApiParam(name = "Bicycles Request", value = "BicyclesRequest Dto", required = false)
      @Valid @RequestBody BicyclesRequest bicyclesRequest) {
    BicyclesResponse bicycleList = bicycleService.obtainBicycles(bicyclesRequest);
    return ResponseEntity.ok().body(bicycleList);
  }
}
