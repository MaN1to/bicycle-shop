package com.bicycleShop.rest.service.interfaces;


import com.bicycleShop.rest.domain.request.BicycleRequest;
import com.bicycleShop.rest.domain.request.BicyclesRequest;
import com.bicycleShop.rest.domain.response.BicycleResponse;
import com.bicycleShop.rest.domain.response.BicyclesResponse;

public interface BicycleService {
  BicycleResponse bicycle(BicycleRequest bicycleRequest);
  BicyclesResponse obtainBicycles(BicyclesRequest bicyclesRequest);
}
