package com.bicycleShop.rest.service.implementation;

import com.bicycleShop.rest.domain.request.BicycleRequest;
import com.bicycleShop.rest.domain.request.BicyclesRequest;
import com.bicycleShop.rest.domain.response.BicycleResponse;
import com.bicycleShop.rest.domain.response.BicyclesResponse;
import com.bicycleShop.rest.helper.Strategy;
import com.bicycleShop.rest.persistence.domain.Bicycle;
import com.bicycleShop.rest.persistence.repository.BicycleRepo;
import com.bicycleShop.rest.service.interfaces.BicycleService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class BicycleServiceImpl implements BicycleService {

  BicycleRepo bicycleRepo;

  private final List<Strategy> bicycleConditions;

  /**
   * @param bicycleRepo
   * @param bicycleConditions
   */
  public BicycleServiceImpl(BicycleRepo bicycleRepo,
      List<Strategy> bicycleConditions) {
    this.bicycleRepo = bicycleRepo;
    this.bicycleConditions = bicycleConditions;
  }

  /**
   * @param bicycleRequest
   * @return
   */
  @Override
  public BicycleResponse bicycle(BicycleRequest bicycleRequest) {

    Bicycle bicycleEntity = Bicycle.buildBicycleEntity(bicycleRequest);
    bicycleRepo.saveAndFlush(bicycleEntity);

    return BicycleResponse.buildBicycleResponse(bicycleEntity);
  }

  /**
   *
   * @return
   */
  @Override
  public BicyclesResponse obtainBicycles(BicyclesRequest bicyclesRequest) {
    List<BicycleResponse> bicycleResponses = bicycleRepo.findAll().stream()
        .map(BicycleResponse::buildBicycleResponse)
        .collect(Collectors.toList());

    List<BicycleResponse> filteredBicycles = bicycleResponses.stream().filter(bicycleResponse ->
        filterBicycles(bicyclesRequest, bicycleResponse))
        .collect(Collectors.toList());

    return new BicyclesResponse(filteredBicycles);
  }

  private boolean filterBicycles(BicyclesRequest bicyclesRequest, BicycleResponse bicycleResponse) {
    return bicycleConditions.stream()
        .anyMatch(thisCondition -> thisCondition.filterBicycle(bicyclesRequest, bicycleResponse));
  }
}
