package com.bicycleShop.rest.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BicyclesRequest {
  private String brand;
  @Pattern(regexp = "29\"|27.5\"|26\"")
  private String wheelDiameter;
  @Pattern(regexp = "20\"|19\"|17.5\"|16\"")
  private String frameSize;
}
