package com.bicycleShop.rest.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiParam;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BicycleRequest {

  @ApiParam(required = true)
  @NotBlank
  private String brand;
  @ApiParam(required = true)
  @NotBlank
  @Pattern(regexp = "29\"|27.5\"|26\"")
  private String wheelDiameter;
  @ApiParam(required = true)
  @NotBlank
  @Pattern(regexp = "HYDRAULIC|MECHANICAL", flags = Pattern.Flag.CASE_INSENSITIVE)
  private String brakeType;
  @ApiParam(required = true)
  @NotBlank
  @Pattern(regexp = "20\"|19\"|17.5\"|16\"")
  private String frameSize;
}
