package com.bicycleShop.rest.domain.response;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BicyclesResponse {

  private List<BicycleResponse> bicycles;
}
