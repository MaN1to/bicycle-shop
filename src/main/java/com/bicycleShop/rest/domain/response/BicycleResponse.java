package com.bicycleShop.rest.domain.response;

import com.bicycleShop.rest.persistence.domain.Bicycle;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BicycleResponse {

  private String brand;
  private String wheelDiameter;
  private String brakeType;
  private String frameSize;

  public static BicycleResponse buildBicycleResponse(Bicycle bicycle) {
    return new BicycleResponse(bicycle.getBrand(), bicycle.getWheelDiameter().replace("\\", ""),
        bicycle.getBrakeType(), bicycle.getFrameSize().replace("\\", ""));
  }
}
