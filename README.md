# Tecnologia Challenge

## Technology used
The challenge was created using the following tools.

  - Java 8
  - Spring Boot
  - Gradle
  - Spring Security
  - Swagger
  - H2 database

## Installation

Follow the next steps to use

```sh
$ ./gradlew build
$ ./gradlew bootRun
```

 - Default port 8080.
 
## Swagger
Use swagger in order to see the posible convinations of operations
 [![N|Solid](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTwdFKqBMgBqxCu2kPtfWAPyWtU4OObc_igMQ&usqp=CAU)](http://localhost:8080/swagger-ui.html)

## H2
This link will lead you to the H2 Database: http://localhost:8080/h2-console/

- driverClassName=org.h2.Driver
- url=jdbc:h2:mem:bicycleShop
- username=root
- password=


## Endpoint Aclarations
For the request related to obtaining the bicycles, we can send the bicycle specification in order to filter theem.
If you want you can filter by Brand, by Frame and by Wheel size. 
In order to set the inches you need to escape the character "(quote) in this way "20\"" (Using the slash).

#### This is an example of the request:
 
  - request
    - http://localhost:8080/shop/obtain
  - body
     ```
    {
        "frame": "29\"",
        "wheelDiameter": "20\""
    }
    ```